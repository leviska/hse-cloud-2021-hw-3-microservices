from common.data_source import CSV
from metasearch.http import Server
from metasearch.service import MetaSearchService
from search.service import SearchInShardsService, SimpleSearchServer
from settings import USER_DATA_FILE, SEARCH_DOCUMENTS_DATA_FILES
from user.service import UserService
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--type', type=str, nargs='?', default="")
parser.add_argument('--port', type=int, nargs='?', default=0)
parser.add_argument('--shard', type=str, nargs='?', default="")
parser.add_argument('--user', type=str, nargs='?', default="")
parser.add_argument('--file', type=str, nargs='?', default="")
parser.add_argument('--search', type=str, nargs='*', default=[])
args = parser.parse_args()

print(args)

def main():
    if args.type == "meta":
        metasearch = MetaSearchService(args.shard, args.user)
        server = Server('metasearch', metasearch=metasearch)
        server.run_server(debug=True)
    elif args.type == "user":
        user_service = UserService(CSV(USER_DATA_FILE))
        user_service.run_server(debug=True)
    elif args.type == "shard":
        shard_service = SearchInShardsService(args.search)
        shard_service.run_server(debug=True)
    elif args.type == "search":
        search_service = SimpleSearchServer(CSV(args.file))
        search_service.run_server(args.port, debug=True)
    else:
        print("durka ebat")

if __name__ == '__main__':
    main()
