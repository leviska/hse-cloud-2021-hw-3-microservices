import requests
from typing import List, Dict

class MetaSearchService:
    def __init__(self, search: str, user_service: str) -> None:
        self._search = search
        self._user_service = user_service

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        user_data = requests.get("http://" + self._user_service + "/get_user_data", {"user_id": user_id}).json()
        res = requests.get("http://" + self._search + "/get_search_data", {"text": search_text, "limit": limit}, data=user_data).json()
        return res
