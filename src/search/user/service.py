import requests
from common.data_source import AbstractDataSource
from flask import Flask, request


class UserService(Flask):
    key = 'user_id'
    data_keys = ('gender', 'age')

    def __init__(self, data_source: AbstractDataSource):
        super().__init__("user")
        self._data = dict()
        data = data_source.read_data()
        for row in data:
            assert row[self.key] not in self._data, f'Key value {self.key}={row[self.key]} is not unique in self._data'
            self._data[row[self.key]] = {k: row[k] for k in self.data_keys}
        self.add_url_rule('/get_user_data', self.get_user_data.__name__, self.get_user_data)

    def get_user_data(self):
        user_id = int(request.args.get('user_id'))
        return self._data.get(user_id)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8002, **kwargs)